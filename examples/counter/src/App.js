import React from 'react';
import { Provider as ReduxProvider } from 'react-redux'
import { Provider as MobxProvider } from 'mobx-react'
import ReduxCounter, { store as reduxStore } from './ReduxCounter' 
import MobxCounter, { store as mobxStore } from './MobxCounter'
import NoLibCounter from './NoLibCounter'

function App() {
  return (
    <div className="App">
      <ReduxProvider store={reduxStore}>
        <ReduxCounter />
      </ReduxProvider>
      <MobxProvider store={mobxStore}>
        <MobxCounter />
      </MobxProvider>
      <NoLibCounter />
    </div>
  );
}

export default App;

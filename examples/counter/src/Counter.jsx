import React from 'react'

export default function Counter({ count, onClickIncrease, onClickDecrease }){
  return (
    <div>
      <button onClick={onClickDecrease}>Decrease</button>
      <span>{count}</span>
      <button onClick={onClickIncrease}>Increase</button>
    </div>
  )
}
import { connect } from 'react-redux'
import { createStore } from 'redux'
import Counter from './Counter'
// This is normally in a seperate File (actionTypes)
const INCREASE_COUNT = 'INCREASE_COUNT'
const DECREASE_COUNT = 'DECREASE_COUNT'
// This is normally in a seperate File (actions)
const increaseCount = () => ({ type: INCREASE_COUNT })
const decreaseCount = () => ({ type: DECREASE_COUNT })
// This is normally in a seperate File (the container container)
const mapStateToProps = count => ({ count })
const mapDispatchToProps = dispatch => ({ 
  onClickIncrease: () => dispatch(increaseCount()),
  onClickDecrease: () => dispatch(decreaseCount())
})

const reducer = (count = 0, action) => {
  switch (action.type) {
    case INCREASE_COUNT:
      return count + 1
    case DECREASE_COUNT:
      return count - 1
    default:
      return count
  }
}

const ReduxCounter = connect(mapStateToProps, mapDispatchToProps)(Counter)
export default ReduxCounter
export const store = createStore(reducer)






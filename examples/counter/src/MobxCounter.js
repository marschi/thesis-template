import { inject, observer } from 'mobx-react'
import { observable, action } from 'mobx'
import Counter from './Counter'

class CounterStore {
  @observable count = 0
  @action increase = () => {
    this.count = this.count + 1
  }
  @action decrease = () => {
    this.count = this.count - 1
  }
}

export const store = new CounterStore()

const mapStoresToProps = ({ store }) => ({
  count: store.count,
  onClickIncrease: store.increase,
  onClickDecrease: store.decrease
})

const MobxCounter = inject(mapStoresToProps)(observer(Counter))
export default MobxCounter
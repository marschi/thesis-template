import React from 'react'
import EventEmitter from 'events'
import Counter from './Counter'

const emitter = new EventEmitter()
const dispatch = (action) => emitter.emit('dispatch', action)
const INCREASE_COUNT = 'INCREASE_COUNT'
const DECREASE_COUNT = 'DECREASE_COUNT'

class CounterStore {

  count = 0

  constructor() {
    emitter.on('dispatch', this.reduce)
  }

  reduce = (action) => {
    switch (action.type) {
      case INCREASE_COUNT:
        this.count = this.count + 1
        break;
      case DECREASE_COUNT:
        this.count = this.count - 1
        break;
      default:
        return
    }
    emitter.emit('update', this.count)
  }
}

const counterStore = new CounterStore()

class NoLibCounter extends React.Component {

  constructor(){
    super()
    this.state = this.getNextState()
    emitter.on('update', () => {
      this.setState(this.getNextState())
    })
  }

  getNextState() {
    return { count: counterStore.count}
  }

  render(){
    return (
      <Counter 
        onClickIncrease={() => dispatch({ type: INCREASE_COUNT })}
        onClickDecrease={() => dispatch({ type: DECREASE_COUNT })}
        count={this.state.count}
      />
    )
  }
}

export default NoLibCounter